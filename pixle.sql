/*
SQLyog Ultimate v9.50 
MySQL - 5.5.5-10.1.10-MariaDB : Database - student_foundation
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`student_foundation` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `student_foundation`;

/*Table structure for table `class` */

DROP TABLE IF EXISTS `class`;

CREATE TABLE `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_code` varchar(50) NOT NULL,
  `class_name` varchar(100) NOT NULL,
  `class_desc` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `class` */

insert  into `class`(`id`,`class_code`,`class_name`,`class_desc`) values (1,'001','English','test'),(2,'00','maths','test'),(3,'thats a secret!','Science','dd'),(4,'thats a secret!','Sinhla','dd'),(5,'thats a secret!','dd','dd'),(12,'pyxle ban','dd','dd'),(13,'thats a secret!','dd','dd'),(14,'007','sds','ds'),(15,'aaaa','aa','aaaa');

/*Table structure for table `grade` */

DROP TABLE IF EXISTS `grade`;

CREATE TABLE `grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_code` varchar(50) NOT NULL,
  `grade_desc` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `grade` */

insert  into `grade`(`id`,`grade_code`,`grade_desc`) values (1,'1','primay'),(2,'2','2nd year'),(3,'dd','fgfgf'),(4,'rerer','rererevdf'),(5,'erere','33333ewfssdf');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `class_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `student` */

insert  into `student`(`id`,`first_name`,`last_name`,`class_id`,`grade_id`) values (1,'dilhan','nakanda',3,5),(2,'saman','wickrma',4,6),(4,'thats a secret!','d33d',1,1),(5,'wwww','ww',2,2),(6,'wwww','ww',2,2),(7,'dias','www',4,3);

/*Table structure for table `tbluser` */

DROP TABLE IF EXISTS `tbluser`;

CREATE TABLE `tbluser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `group_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbluser` */

insert  into `tbluser`(`id`,`username`,`email`,`password`,`group_id`) values (1,'admin','admin@gmail.com','123456',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
