<?php


Route::group(['middleware' => ['web']], function () {


    Route::group(array('prefix' => 'api'), function()
    {


        Route::post('/sss', function () {
            return json_encode(array( 'status' => 'success', 'message' => 'Stundet Information API is working fine'  ));



        });

        Route::post('/s3', function () {

            //var_dump($_POST);
            return json_encode(array( 'status' => 'success', 'message' => 's3 API is working fine'  ));



        });


        Route::post('/addclass', 'UserController@addClass');
        Route::post('/addGrade', 'UserController@addGrade');
        Route::post('/addStudent', 'UserController@addStudent');
        Route::post('/listStudent', 'UserController@listStudent');
        Route::post('/listClass', 'UserController@listClass');


    });


    Route::get('/', function () {


        return view('welcome');




    });

    Route::get('/test', 'UserController@login');

    Route::post('/transaction','UserController@transAction');
    Route::get('/testCode', 'UserController@testCode');
    Route::get('/show', 'UserController@show');

       // $request = Request::create('http://localhost/pixle/api/public/', 'GET');
      //  $request = \Illuminate\Http\Request::create('http://localhost/pixle/api/public/', 'GET', ['param1' => 'value1', 'param2' => 'value2']);
    //  echo "<pre>". print_r($request,1) . "</pre>";
        //     return view('welcome');







  //  $request = \Illuminate\Http\Request::create('http://your-api.com', 'POST', ['param1' => 'value1', 'param2' => 'value2']);



});