<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome to Foundation </title>

    <!-- Bootstrap Core CSS -->
     <link href="{{URL::asset('css/bootstrap.min.css') }}" rel="stylesheet"> 

    <!-- Custom CSS -->
      <link href="{{URL::asset('css/simple-sidebar.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
textarea {
resize: none;
}
</style>
</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">

                <li>
                    <a href="students">STUDENTS</a>
                </li>
                <li>
                    <a href="classes">CLASSES</a>
                </li>
                <li>
                    <a href="grades">GRADES</a>
                </li>
               
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2> Add Grade</h2>
                         <form style="width: 450px;">
  <div class="form-group">
    <label for="exampleInputGcode">Grade Code</label>
    <input type="email" class="form-control" id="exampleInputGcode" placeholder="Enter Grade Code">
    </div>
  
 <div class="form-group">
    <label for="exampleGradeDesc">Grade Description</label>
    <textarea class="form-control" id="exampleGradeDesc" rows="3" ></textarea>
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>
</form>               
                      <!--  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Hide Menu</a> -->
					
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>
