<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Grade extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "grade";
    protected $primaryKey = "id";
    protected $fillable = [
        'grade_code', 'grade_desc',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
