<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class tblClass extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "class";
    protected $primaryKey = "id";
    protected $fillable = [
        'class_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
