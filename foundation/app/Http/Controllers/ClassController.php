<?php

namespace App\Http\Controllers;
use App\Grade;
use App\tblClass;
use Form;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;

class ClassController extends Controller
{
   
    public function showClassPage()
    {

        $data['results'] = "";
        $client = new Client;

        $url = 'http://localhost/pixle/api/public/api/listClass';

        $res = $client->post($url, []);


        $obj = json_decode($res->getBody());

        if($obj->status->status == "success"){

            $data['results']  =  $obj->details;

        }
        else {

            echo   $obj->status->message;
        }



        return view('classes',$data);
    }

    public function addClassPage()
    {
        return view('add_class');
    }

    public function saveClass() {


        $client = new Client;


        $url = 'http://localhost/pixle/api/public/api/addclass';

        $res = $client->post($url, ['json' => [
            "class_code" =>  Input::get("class_code"),
            "class_name" =>  Input::get("class_name"),
            "class_desc" =>  Input::get("class_desc")

        ]]);

        $obj = json_decode($res->getBody());

        if($obj->status->status == "success"){


            return Redirect::to("/classes");
        }
        else {

            echo   $obj->status->message;
        }





    }


}