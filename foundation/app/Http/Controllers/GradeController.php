<?php

namespace App\Http\Controllers;

class GradeController extends Controller
{
   
    public function showGradePage()
    {
        return view('grades');
    }

     public function addGradePage()
    {
        return view('add_grade');
    }
}