<?php

namespace App\Http\Controllers;

use App\Grade;
use App\tblClass;
use Form;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
class StudentController extends Controller
{
   
    public function showStudentPage()
    {

        $data['results'] = "";
        $client = new Client;

        $url = 'http://localhost/pixle/api/public/api/listStudent';

        $res = $client->post($url, []);


        $obj = json_decode($res->getBody());

        if($obj->status->status == "success"){

            $data['results']  =  $obj->details;

        }
        else {

            echo   $obj->status->message;
        }





        return view('students',$data);
    }

     public function addStudentPage()
    {
        $tblgrade = collect(grade::lists("grade_desc", "id"));
        $data['tblgrade'] =$tblgrade;
        $tblclass = collect(tblClass::lists("class_name", "id"));
        $data['tblclass'] =$tblclass;

        return view('add_student',$data);
    }

    public function SaveStudent()
    {
        $client = new Client;


        $url = 'http://localhost/pixle/api/public/api/addStudent';

        $res = $client->post($url, ['json' => [
            "first_name" =>  Input::get("first_name"),
            "last_name" =>  Input::get("last_name"),
            "class_id" =>  Input::get("class_id"),
            "grade_id" =>  Input::get("grade_id")
        ]]);

        $obj = json_decode($res->getBody());

        if($obj->status->status == "success"){


            return Redirect::to("/students");
        }
        else {

            echo   $obj->status->message;
        }



      //  return view('add_student',$data);
    }




}