<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Request;
use Validator;

class UserController extends Controller {

    public function login() {
        $client = new Client;

        $res = $client->get('http://localhost/pixle/api/public/', ['json' => [
            "access_token" => "thats a secret!",
            "another_payload" => 'dd'
        ]]);

        //echo $res->getStatusCode();
        // echo $res->getHeader('content-type');
        echo $res->getBody();
        $obj = $res->getBody();
        echo "<br>";
        $obj = json_decode($obj);

        var_dump($obj->status);

        /* $r = $client->post('https://api.apiprovider.com:9900/create_task',
          ['json' => [
          "access_token" =>"thats a secret!",
          "another_payload" =>  'dd'
          ]]); */
    }

    public function transAction() {

        $data['status'] = array(
            'status' => 'fail',
            'message' => 'Invalid reset code.'
        );
        //
        return $data;
    }

    public function testCode() {


        $client = new Client;

        $url = 'http://localhost/pixle/api/public/api/addStudent';

        $res = $client->post($url, ['json' => [
            "first_name" => "thats a secret!",
            "last_name" => 'd33d',
            "class_id" => 1,
            "grade_id" => 1
        ]]);

        $obj = json_decode($res->getBody());

        if($obj->status->status == "success"){


            echo $obj->status->message;
        }
        else {

            echo   $obj->status->message;
        }






    }

    public function addClass() {
        $input = Request::json();

        $validator = Validator::make($input->all(), [
            'class_code' => 'required',
            'class_name' => 'required',
            'class_desc' => 'required'
        ]);

        if ($validator->fails()) {

            $data['status'] = array(
                'status' => 'fail',
                'message' => 'Error Occured'
            );
        } else {

            $data['status'] = array(
                'status' => 'success',
                'message' => "Your class has been successfully created."
            );
        }

        return $data;
    }

}
