<?php


Route::group(['middleware' => ['web']], function () {

Route::get('/students', 'StudentController@showStudentPage');
Route::get('/add_student', 'StudentController@addStudentPage');
Route::get('/classes', 'ClassController@showClassPage');
Route::get('/add_class', 'ClassController@addClassPage');
Route::get('/grades', 'GradeController@showGradePage');
Route::get('/add_grade', 'GradeController@addGradePage');


    Route::post('/save_student', 'StudentController@SaveStudent');
    Route::post('/save_classes', 'ClassController@saveClass');

  Route::get('/', function () {
     return view('welcome');




    });
    Route::get('/testCode', 'UserController@testCode');

});