<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = ['api/s3','api/addclass', 'save_student',
        'api/addStudent','api/listStudent','api/addGrade','/save_classes', 'api/listClass'
        //
    ];



}
